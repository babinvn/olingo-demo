package me.bvn.sample.olingo;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

import org.apache.olingo.odata2.api.processor.ODataContext;
import org.apache.olingo.odata2.jpa.processor.api.ODataJPAContext;
import org.apache.olingo.odata2.jpa.processor.api.ODataJPAServiceFactory;
import org.apache.olingo.odata2.jpa.processor.api.exception.ODataJPARuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * ODataJPAServiceFactory implementation for our sample domain 
 * @author Philippe
 *
 */
@Component
public class CarsODataJPAServiceFactory extends ODataJPAServiceFactory {
    private static final Logger log = LoggerFactory.getLogger(CarsODataJPAServiceFactory.class);

    public CarsODataJPAServiceFactory() {
        // Enable detailed error messages (useful for debugging)
        setDetailErrors(true);
    }

    /**
     * This method will be called by Olingo on every request to
     * initialize the ODataJPAContext that will be used. 
     */
    @Override
    public ODataJPAContext initializeODataJPAContext() throws ODataJPARuntimeException {

        log.info("[I32] >>> initializeODataJPAContext()");
        ODataJPAContext ctx = getODataJPAContext();
        ODataContext octx = ctx.getODataContext();
        HttpServletRequest request = (HttpServletRequest)octx.getParameter(ODataContext.HTTP_SERVLET_REQUEST_OBJECT);
        EntityManager em = (EntityManager)request.getAttribute(JerseyConfig.EntityManagerFilter.EM_REQUEST_ATTRIBUTE);
                
        // Here we're passing the EM that was created by the EntityManagerFilter (see JerseyConfig)
        ctx.setEntityManager(new EntityManagerWrapper(em));
        ctx.setPersistenceUnitName("default");
        
        // We're managing the EM's lifecycle, so we must inform Olingo that it should not
        // try to manage transactions and/or persistence sessions
        ctx.setContainerManaged(true);                
        return ctx;
    }
}