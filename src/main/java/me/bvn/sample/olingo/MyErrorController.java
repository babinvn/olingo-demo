package me.bvn.sample.olingo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import static org.springframework.boot.web.error.ErrorAttributeOptions.Include.EXCEPTION;
import static org.springframework.boot.web.error.ErrorAttributeOptions.Include.MESSAGE;
import static org.springframework.boot.web.error.ErrorAttributeOptions.Include.STACK_TRACE;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@RestController
public class MyErrorController implements ErrorController {
    @Value("${app.debug.enabled}")
    private boolean debug;

    @Autowired
    private ErrorAttributes errorAttributes;
    @Autowired
    private ObjectMapper mapper;

    @GetMapping("/error")
    @Produces("application/json")
    public ResponseEntity<?> error(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.valueOf(status.toString());
            if (statusCode == 404) {
                // Redirect to index.html for Vuejs history routing mode
                response.sendRedirect(String.format("%s/index.html", request.getContextPath()));
                return null;
            }
        }

        WebRequest webRequest = new ServletWebRequest(request);
        Map<String, Object> attrs = debug?
            errorAttributes.getErrorAttributes(webRequest, ErrorAttributeOptions.of(EXCEPTION, STACK_TRACE, MESSAGE)):
            errorAttributes.getErrorAttributes(webRequest, ErrorAttributeOptions.of(EXCEPTION, MESSAGE));

        ObjectNode result = mapper.createObjectNode();
        result.putPOJO("status", status);
        attrs.forEach((key, value) -> result.putPOJO(key, value));
        int statusCode = status == null? 500: Integer.valueOf(status.toString());
        return new ResponseEntity<>(result, HttpStatus.valueOf(statusCode));
    }

    @Override
    public String getErrorPath() {
        return null;
    }
}
