# Demo OData application

This is a demo OData with Jersey + Olingo2 backend and Vuejs + DevExtreme frontend.

To build you need JDK8+, gradle and node

## Frontend

Use Vue CLI to create frontend:

```
npx -p @vue/cli vue create olingo-demo
mv olingo-demo frontend
cd frontend
```

Use DevExtreme CLI to add components to project:

```
npx -p devextreme-cli devextreme add devextreme-vue
```

Use [vue.config.js](frontend/vue.config.js) to make this app run on `/app` path.

Create a [page](frontend/src/views/Grid.vue) with [DevExtreme Data Grid](https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/)
that uses backend OData endpoint `/app/odata/CarMakers`.

## Backend

Shamelessly stolen from https://www.baeldung.com/olingo. Please check original article for details.

Navigate to [SpringBoot Starter](https://start.spring.io) and create a new project
with Jersey, JPA and H2 dependencies. Add Olingo2 to your project:

```
dependencies {
	compile ('org.apache.olingo:olingo-odata2-core:2.0.11') {
		exclude group: 'javax.ws.rs', module: 'javax.ws.rs-api'
	}
	compile ('org.apache.olingo:olingo-odata2-jpa-processor-core:2.0.11') {
		exclude group: 'javax.ws.rs', module: 'javax.ws.rs-api'
	}
	compile ('org.apache.olingo:olingo-odata2-jpa-processor-ref:2.0.11') {
		exclude group: 'org.eclipse.persistence', module: 'eclipselink'
	}
```

Excluding transient dependencies is important, otherwise your application will
fail at runtime.

To build:

```
gradle build
```

To run:

```
java -Dserver.port=8080 -jar build/libs/olingo-demo-0.0.1-SNAPSHOT.jar
```

To run in debug mode:

```
java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 -jar build/libs/olingo-demo-0.0.1-SNAPSHOT.jar
```

Navigate to `http://localhost:8080/app/grid` to see the grid page.
